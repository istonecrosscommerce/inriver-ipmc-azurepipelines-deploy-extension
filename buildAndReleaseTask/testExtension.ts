import { apiPaths } from "./apiPaths";
import { getErrorMessage } from "./getErrorMessage";
import { IExtensionResult } from "./interfaces/IExtensionResult";
import { ITestResult } from "./interfaces/ITestResults";

export async function testExtension(
  extension: IExtensionResult,
  defaultAcceptMessage: string,
  axiosInstance: any
) {
  console.log(
    `Executing and verifying Test method of: ${extension.extensionId}`
  );
  let testResult: any;
  const returnResult: ITestResult = {
    successfullyMatched: false,
    message: "",
  };
  try {
    testResult = await axiosInstance({
      method: "post",
      url: apiPaths.extensionTest(extension.extensionId),
    });
  } catch (err) {
    let message = getErrorMessage(err);
    console.log("Error when testing: " + message);
    returnResult.message = message;
    return returnResult;
  }

  const message = testResult?.data?.message as string;
  if (message === undefined) {
    const error = `Unable to retrieve message from test method of: ${extension.extensionId}`;
    returnResult.message = error;
    return returnResult;
  }

  const regExToUse = extension.allowedTestResponses
    ? extension.allowedTestResponses
    : defaultAcceptMessage;
  const isMatch = message.match(regExToUse);
  if (isMatch === null) {
    returnResult.message = `The message: ${message} from ${extension.extensionId} doesn't match with RegEx: ${regExToUse}`;
    console.log(returnResult.message);
  } else {
    returnResult.successfullyMatched = true;
  }

  return returnResult;
}
