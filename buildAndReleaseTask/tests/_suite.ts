import * as assert from 'assert';
import * as ttm from 'azure-pipelines-task-lib/mock-test';
import * as path from 'path';
import { getExistingExtension } from '../extensionAlreadyExists';
import { flattenExtensionConfig } from '../flattenExtensionConfig';
import IExtensionConfig from '../interfaces/IExtensionConfig';
import IFlattenedExtensionConfig from '../interfaces/IFlattenedExtensionConfig';
import Package from '../interfaces/Package';
import tmrm = require('azure-pipelines-task-lib/mock-run');

describe('api tests', function () {

    beforeEach(() => {    
    });

    afterEach(() => {
    });

    it('should succeed when test string can be validated with a custom regEx', (done:Mocha.Done) => {

        let tp = path.join(__dirname, 'correct_when_using_custom_test_pattern.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        
        console.log("log verification")
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        assert.equal(tr.succeeded, true, 'should have succeeded');
        done();
    });

    it('should have failed if not given all the parameters', (done:Mocha.Done) => {
        
        let tp = path.join(__dirname, 'failure.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        assert.equal(tr.succeeded, false, 'should have failed');
        done();
    });

    it('should have worked correctly when extensions exist', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'correct_when_extensions_exist.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");
        assert.equal(tr.warningIssues.length, 0, "should have no warnings");
        assert.equal(tr.errorIssues.length, 0, "should have no errors");
        done();
    });

    it('should have worked correctly when extensions exist and 1 should be disabled', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'correct_when_extensions_exist_and_should_be_disabled.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");
        assert.equal(tr.warningIssues.length, 0, "should have 1 warnings");
        assert.equal(tr.errorIssues.length, 0, "should have no errors");
        done();
    });

    it('should have worked correctly when given a timeout the first time', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'correct_when_timeout_first.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");
        assert.equal(tr.warningIssues.length, 0, "should have no warnings");
        assert.equal(tr.errorIssues.length, 0, "should have no errors");
        done();
    });

    it('should have worked correctly when not appending extension', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'correct_when_notAppendingExtension.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");
        assert.equal(tr.warningIssues.length, 0, "should have no warnings");
        assert.equal(tr.errorIssues.length, 0, "should have no errors");
        done();
    });

    it('should have failed when test methods dont match expected result', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'fail_when_test_method_returns_wrong.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");
        assert.equal(tr.warningIssues.length, 1, "should have 1 warnings");
        assert.equal(tr.errorIssues.length, 0, "should have 3 errors");
        done();
    });
    
    it('should have worked correctly when extensions doesnt exist', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'correct_when_no_extensions_exist.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");
        assert.equal(tr.warningIssues.length, 0, "should have no warnings");
        assert.equal(tr.errorIssues.length, 0, "should have no errors");        
        done();
    });

    it('should have worked correctly when waiting after restartservice', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'correct_when_waiting_after_restart.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        var preTime = new Date().getTime();
        tr.run();
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        var postTime = new Date().getTime();
        const timeTaken = (postTime - preTime);
        assert.equal(timeTaken > 1000, true, 'Should take longer then 1000 ms. Took: ' + timeTaken + ' milliseconds ');
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");
        assert.equal(tr.warningIssues.length, 0, "should have no warnings");
        assert.equal(tr.errorIssues.length, 0, "should have no errors");        
        done();
    });

    it('should have worked correctly when given a folder structure', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'correct_with_folder_structure.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");
        assert.equal(tr.warningIssues.length, 0, "should have no warnings");
        assert.equal(tr.errorIssues.length, 0, "should have no errors");        
        done();
    });

    it('should have worked correctly when given a folder structure and exclude option', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'correct_with_folder_structure_excluding.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");
        assert.equal(tr.warningIssues.length, 0, "should have no warnings");
        assert.equal(tr.errorIssues.length, 0, "should have no errors");        
        done();
    });

    it('should have worked correctly when given a complex exclude situation in folder structure', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'correct_with_folder_structure_complex_exclude.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");
        assert.equal(tr.warningIssues.length, 0, "should have no warnings");
        assert.equal(tr.errorIssues.length, 0, "should have no errors");        
        done();
    });

    it('should have worked correctly when given an incorrect folder structure', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'correct_with_incorrect_folderstructure.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");
        assert.equal(tr.warningIssues.length, 0, "should have no warnings");
        assert.equal(tr.errorIssues.length, 0, "should have no errors");        
        done();
    });

    it('should have worked correctly when given a folder structure and filename', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'correct_with_folder_structure_and_filename.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");
        assert.equal(tr.warningIssues.length, 0, "should have no warnings");
        assert.equal(tr.errorIssues.length, 0, "should have no errors");        
        done();
    });

    it('should have worked correct when settings already exists and are equal', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'correct_when_comparing_settings_and_settings_are_equal.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");
        assert.equal(tr.warningIssues.length, 0, "should have no warnings");
        assert.equal(tr.errorIssues.length, 0, "should have no errors");        
        done();
    });

    it('should have worked correct when some settings already exists and are equal', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'correct_when_comparing_settings_and_some_settings_are_equal.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");
        assert.equal(tr.warningIssues.length, 0, "should have no warnings");
        assert.equal(tr.errorIssues.length, 0, "should have no errors");        
        done();
    });

    it('should have executed all test methods when one test timeouts', (done:Mocha.Done) => {
        this.timeout(10000);
        let tp = path.join(__dirname, 'correct_with_when_one_test_timeouts.js');
        let tr:ttm.MockTestRunner = new ttm.MockTestRunner(tp);
        tr.run();
        if (!tr.succeeded) {
            console.log(tr.stdout);
        }
        assert.equal(tr.succeeded, true, 'should have succeeded (' + JSON.stringify(tr.errorIssues) + ")");        
        assert.equal(tr.warningIssues.length, 1, "should have 1 warning from test methods.");
        assert.equal(tr.errorIssues.length, 0, "should have no errors");        
        done();
    });

});

describe('function tests', function () {
    it('find that existing extension', (done:Mocha.Done) => {
        let existingExtensions:any = [
            {
                "extensionId": "test1"
            },
            {
                "extensionId": "test2"
            },
            {
                "extensionId": "test3"
            }
        ]

        let currentExtension:IFlattenedExtensionConfig = {
            "extensionId": "test2",
            "extensionType": "string",
            "assemblyName": "string",
            "assemblyType": "string",
            "isEnabled": false,
            "isPaused": false,
            "apiKey": "string",
            "packageName": "string",
            "packageId": 23,
            "settings": { 
                "test1":"value1"
            }
        }
        let result = getExistingExtension(currentExtension, existingExtensions);

        assert.strictEqual(result !== undefined, true, 'should have found it');
        done();
    });

    it('dont find that non-existing extension', (done:Mocha.Done) => {
        let existingExtensions:any = [
            {
                "extensionId": "test1"
            },
            {
                "extensionId": "test2"
            },
            {
                "extensionId": "test3"
            }
        ]

        let currentExtension:IFlattenedExtensionConfig = {
            "extensionId": "test4",
            "extensionType": "string",
            "assemblyName": "string",
            "assemblyType": "string",
            "isEnabled": false,
            "isPaused": false,
            "apiKey": "string",
            "packageName": "string",
            "packageId": 23,
            "settings": { 
                "test1":"value1"
            }
        }
        let result = getExistingExtension(currentExtension, existingExtensions);

        assert.strictEqual(result === undefined, true, 'shouldnt have found it');
        done();
    });
    it('should flatten the extension config correctly', (done:Mocha.Done) => {
        
        let extensionConfig:IExtensionConfig[] = [{
            "extensionId": "inRiverTrainingRestApi",
            "extensionType": ["LinkListener", "EntityListener"],
            "assemblyName": "inRiver.Training.RestApi.dll",
            "assemblyType": "inRiver.Training.RestApi.RestApiTest",
            "isEnabled": true,
            "isPaused": false,
            "apiKey": "exampleApiKey",
            "packageName": "example.zip",
            "packageId": undefined,
            "settings": {
                "testSetting1": "value1",
                "testSetting2": "value2"
            }
        }, {
            "extensionId": "inRiverTrainingRestApi",
            "extensionType": "InboundDataExtension",
            "assemblyName": "inRiver.Training.RestApi.dll",
            "assemblyType": "inRiver.Training.RestApi.RestApiTest",
            "isEnabled": true,
            "isPaused": false,
            "packageId": undefined,
            "apiKey": "inRiverTrainingKey",
            "packageName": "example.zip",
            "settings": {
                "testSetting1": "value1",
                "testSetting2": "value3"
            }
        }]

        let packages:Package[] = [
            {
                "packageName": "example.zip",
                "packageId": 23,
                "deployed": false,
                "failedToDelete": false,
                "failedPackageId": -1
    
            }
        ]
        

        let result = flattenExtensionConfig(extensionConfig, packages);

        assert.strictEqual(result.length, 3, 'Should have flattened into 3 extensions');
        
        assert.strictEqual(result[0].packageId, packages[0].packageId, 'should have the correct packageId');
        assert.strictEqual(result[1].packageId, packages[0].packageId, 'should have the correct packageId');
        assert.strictEqual(result[2].packageId, packages[0].packageId, 'should have the correct packageId');

        assert.strictEqual(result[0].extensionId, 'inRiverTrainingRestApiLiLi', 'should have a correct extensionid');
        assert.strictEqual(result[1].extensionId, 'inRiverTrainingRestApiEnLi', 'should have a correct extensionid');
        assert.strictEqual(result[2].extensionId, 'inRiverTrainingRestApiInDaEx', 'should have a correct extensionid');

        done();
    });
    it('should not append the extension type', (done:Mocha.Done) => {
        
        let extensionConfig:IExtensionConfig[] = [ {
            "extensionId": "inRiverTrainingRestApi",
            "extensionType": "InboundDataExtension",
            "assemblyName": "inRiver.Training.RestApi.dll",
            "assemblyType": "inRiver.Training.RestApi.RestApiTest",
            "isEnabled": true,
            "isPaused": false,
            "packageId": undefined,
            "apiKey": "inRiverTrainingKey",
            "packageName": "example.zip",
            "settings": {
                "testSetting1": "value1",
                "testSetting2": "value3"
            },
            "notAppendExtensionType" : true
        }]

        let packages:Package[] = [
            {
                "packageName": "example.zip",
                "packageId": 23,
                "deployed": false,
                "failedToDelete": false,
                "failedPackageId": -1
    
            }
        ]
        

        let result = flattenExtensionConfig(extensionConfig, packages);

        assert.strictEqual(result[0].extensionId, 'inRiverTrainingRestApi', 'should have a correct extensionid');

        done();
    });
});