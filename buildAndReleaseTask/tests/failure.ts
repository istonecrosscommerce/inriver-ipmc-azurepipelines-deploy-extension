import ma = require('azure-pipelines-task-lib/mock-answer');
import tmrm = require('azure-pipelines-task-lib/mock-run');
import path = require('path');
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';


let taskPath = path.join(__dirname, '..', 'index.js');
let tmr: tmrm.TaskMockRunner = new tmrm.TaskMockRunner(taskPath);

var mock = new MockAdapter(axios, { delayResponse: 0 });

tmr.setInput('samplestring', 'bad');
tmr.registerMock("./axiosInstance", (apiUrl:string, apiKey:string) => { return axios } );

tmr.run();