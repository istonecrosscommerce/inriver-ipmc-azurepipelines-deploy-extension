import ma = require('azure-pipelines-task-lib/mock-answer');
import tmrm = require('azure-pipelines-task-lib/mock-run');
import path = require('path');
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { apiPaths } from '../apiPaths';

const packagesGet = [
    {
    "id": 1,
    "createdDate": "string",
    "modifiedDate": "string",
    "fileName": "example.zip",
    "version": 1
    }
]

const extensionsGet = new Array();

const settingsGet = new Array();

let taskPath = path.join(__dirname, '..', 'index.js');
let tmr: tmrm.TaskMockRunner = new tmrm.TaskMockRunner(taskPath);

var mock = new MockAdapter(axios, { delayResponse: 0 });

let successFullyDeletedPackage:boolean = false;

let extensionsCreated = 0;

mock.onGet(apiPaths.packages).reply(200, packagesGet).onPost(apiPaths.uploadandreplacebase64(1)).reply(200, { id: 2 });
mock.onGet(apiPaths.extensions).reply(200, extensionsGet).onGet(/extensions\/\w+\/settings/).reply(404);


mock.onPost(apiPaths.extensions).reply(() => {
    extensionsCreated++;
    if (extensionsCreated > 6) return [500, 'Shouldnt create more then 4 extensions']
    return [200, {}]
}).onGet(/extensions\/[\w\.]+\/settings/).reply(200, settingsGet);

mock.onPut(/extensions\/[\w\.]+\/settings/).reply(200);       
mock.onPost(/extensions\/[\w\.]+\/settings:reload/).reply(204);
mock.onPut(/extensions\/[\w\.]+\/apikey/).reply(200);       
mock.onPost(/extensions\/[\w\.]+:test/).reply(200, { message: "Success! Extension.ExtensionId is running, version: 1.0.0.0"});
mock.onPost(/extensionmanager:restartservice/).reply(200);

mock.onAny().reply(function(config) {
    console.log("Api call was not supposed to happen: " + config.url + " (" + config.method + ")");
    throw new Error('Api call was not supposed to happen: ' + config.url + " (" + config.method + ")");            
});

let examplePath = path.join(process.cwd(), "/tests/examples-with-folderstructure-exclude/");

tmr.setInput('packagePath', examplePath);
tmr.setInput('extensionConfigPath', examplePath);
tmr.setInput('apiUrl', 'https://example.com/');
tmr.setInput('apiKey', 'b5101c196bfba8254ee4416691ed97c8');
tmr.setInput('environmentName', 'test');
tmr.setInput('defaultAcceptedTestMessage', '^Success');
tmr.setInput('excludePattern', "ExcludeThisFolder")

tmr.registerMock("./axiosInstance", (apiUrl:string, apiKey:string) => { return axios } );

tmr.run();