import ma = require('azure-pipelines-task-lib/mock-answer');
import tmrm = require('azure-pipelines-task-lib/mock-run');
import path = require('path');
import { apiPaths } from '../apiPaths';
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';

const packagesGet = [
    {
    "id": 1,
    "createdDate": "string",
    "modifiedDate": "string",
    "fileName": "example.zip",
    "version": 1
    }
]

let taskPath = path.join(__dirname, '..', 'index.js');
let tmr: tmrm.TaskMockRunner = new tmrm.TaskMockRunner(taskPath);

var mock = new MockAdapter(axios, { delayResponse: 0 });

let successFullyDeletedPackage:boolean = false;

mock.onGet(apiPaths.packages).reply(200, packagesGet);
mock.onPost(apiPaths.uploadbase64).reply(200, { id: 2 });
mock.onDelete(/extensions\/\w+/).reply(200);    
mock.onPost(apiPaths.extensions).reply(200);
mock.onPost(/packages\/\w+:uploadandreplacebase64/).reply(() => { 
    successFullyDeletedPackage = true;
    return [200, { id: 2 }];
});
mock.onPut(/extensions\/\w+\/apikey/).reply(200);       
mock.onPut(/extensions\/\w+\/settings/).reply(200);

mock.onAny().reply(function(config) {
    console.log("Mock not specified: " + config.url + " (" + config.method + ")");
    throw new Error('Could not find mock for ' + config.url);            
});

let examplePath = path.join(process.cwd(), "/tests/examples/");

tmr.setInput('packagePath', examplePath);
tmr.setInput('extensionConfigPath', examplePath);
tmr.setInput('apiUrl', 'https://example.com/');
tmr.setInput('apiKey', 'b5101c196bfba8254ee4416691ed97c8');
tmr.setInput('environmentName', 'test');

tmr.registerMock("./axiosInstance", (apiUrl:string, apiKey:string) => { return axios } );

tmr.run();