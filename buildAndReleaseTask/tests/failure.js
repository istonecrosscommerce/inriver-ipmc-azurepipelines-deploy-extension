"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var tmrm = require("azure-pipelines-task-lib/mock-run");
var path = require("path");
var axios_1 = __importDefault(require("axios"));
var axios_mock_adapter_1 = __importDefault(require("axios-mock-adapter"));
var taskPath = path.join(__dirname, '..', 'index.js');
var tmr = new tmrm.TaskMockRunner(taskPath);
var mock = new axios_mock_adapter_1.default(axios_1.default, { delayResponse: 0 });
tmr.setInput('samplestring', 'bad');
tmr.registerMock("./axiosInstance", function (apiUrl, apiKey) { return axios_1.default; });
tmr.run();
