import ma = require('azure-pipelines-task-lib/mock-answer');
import tmrm = require('azure-pipelines-task-lib/mock-run');
import path = require('path');
import axios, { AxiosRequestConfig } from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { apiPaths } from '../apiPaths';
import { setupInterceptors } from '../axiosRetryInterceptor';

const packagesGet = [
    {
    "id": 1,
    "createdDate": "string",
    "modifiedDate": "string",
    "fileName": "example.zip",
    "version": 1
    }
]

const extensionsGet = [
    {
        "extensionId": "inRiverTrainingRestApiLiLi",
        "extensionType": "LinkListener",
        "status": {
            "isEnabled": true,
            "isPaused": false
        }
    },
    {
        "extensionId": "inRiverTrainingRestApiEnLi",
        "extensionType": "EntityListener",
        "status": {
            "isEnabled": true,
            "isPaused": false
        }
    },
    {
        "extensionId": "inRiverDisabledExtensionInDaEx",
        "extensionType": "InboundDataExtension",
        "status": {
            "isEnabled": true,
            "isPaused": false
        }
    }
]

const settingsGet = [
    {
        "key": "Setting1",
        "value": "Value1"
    }
]

let taskPath = path.join(__dirname, '..', 'index.js');
let tmr: tmrm.TaskMockRunner = new tmrm.TaskMockRunner(taskPath);

setupInterceptors(axios);

var mock = new MockAdapter(axios, { delayResponse: 0 });

let successFullyDeletedPackage:boolean = false;
let successfullyDisabledExtension:boolean = false;

let settingsFetched = 0;

mock.onGet(apiPaths.packages).reply(200, packagesGet).onPost(apiPaths.uploadandreplacebase64(1)).reply(200, { id: 2 });
mock.onGet(apiPaths.extensions).reply(200, extensionsGet).onGet(/extensions\/\w+\/settings/).reply(() => {
    settingsFetched++;
    if (settingsFetched > extensionsGet.length) {
        return [404, {}];
    }
    return [200, settingsGet];
}).onDelete(/extensions\/\w+\/settings\/Setting1/).reply(200, {}).onPut(/extensions\/\w+\/settings/).reply(200);

mock.onPost(/extensions\/inRiverDisabledExtensionInDaEx+:disable/).reply(() => {     
    successfullyDisabledExtension = true;
    return [200];
})

mock.onPut(/extensions\/\w+\/apikey/).reply(200);       
mock.onPost(/extensions\/\w+\/settings:reload/).reply(204);
mock.onPost(/extensions\/\w+:test/).reply((config:AxiosRequestConfig) => {
    if (config !== undefined && config.url !== undefined) {
        if (config.url.indexOf("inRiverDisabledExtensionInDaEx") > -1) {
            console.log(`Extension ${config.url} should not be tested, it is disabled!`);
            throw new Error(`Extension ${config.url} should not be tested, it is disabled!`);    
        }
    }
    return [200, { message: "Success! Extension.ExtensionId is running, version: 1.0.0.0"}];
});
mock.onPost(/extensionmanager:restartservice/).reply(() => { 
    if (successfullyDisabledExtension) {
        return [200];
    }
    console.log("Extension was not correctly disabled.");
    throw new Error('Extension was not correctly disabled.');
});

mock.onAny().reply(function(config) {
    console.log("Api call was not supposed to happen: " + config.url + " (" + config.method + ")");
    throw new Error('Api call was not supposed to happen: ' + config.url + " (" + config.method + ")");            
});

let examplePath = path.join(process.cwd(), "/tests/examples-with-disabled/");

tmr.setInput('packagePath', examplePath);
tmr.setInput('extensionConfigPath', examplePath);
tmr.setInput('apiUrl', 'https://example.com/');
tmr.setInput('apiKey', 'b5101c196bfba8254ee4416691ed97c8');
tmr.setInput('environmentName', 'test');
tmr.setInput('defaultAcceptedTestMessage', '^Success');

tmr.registerMock("./axiosInstance", (apiUrl:string, apiKey:string) => { return axios } );

tmr.run();