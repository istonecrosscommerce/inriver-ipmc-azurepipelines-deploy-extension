import ma = require('azure-pipelines-task-lib/mock-answer');
import tmrm = require('azure-pipelines-task-lib/mock-run');
import path = require('path');
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { apiPaths } from '../apiPaths';
import fs = require('fs');

let examplePath = path.join(process.cwd(), "/tests/examples-with-folderstructure-and-larger-filesettings/");

const packagesGet = [
    {
    "id": 1,
    "createdDate": "string",
    "modifiedDate": "string",
    "fileName": "example.zip",
    "version": 1
    }
]

const extensionsGet = new Array();

//Extension.AnotherIdInDaEx
const settingsAnotherIdInDaExGet = [
    {
        "key": "testSetting1",
        "value": "value1"
    },
    {
        "key": "testSetting2",
        "value": "value3"
    },
]

let settingsContent = fs.readFileSync(path.join(examplePath, "FolderWithExtensions/Extension.AnotherId.Tests/.config/testfile.anything"), 'utf8');

const settingsListenerExtensionIdGet = [
    {
        "key": "testSetting2",
        "value": "value2"
    },
    {
        "key": "testFileSetting1",
        "value": settingsContent
    },
]



let taskPath = path.join(__dirname, '..', 'index.js');
let tmr: tmrm.TaskMockRunner = new tmrm.TaskMockRunner(taskPath);

var mock = new MockAdapter(axios, { delayResponse: 0 });

let successFullyDeletedPackage:boolean = false;

let extensionsCreated = 0;

let listenerExtensionSettingsFetched = false;
let extensionSettingsFetched = false;

mock.onGet(apiPaths.packages).reply(200, packagesGet).onPost(apiPaths.uploadandreplacebase64(1)).reply(200, { id: 2 });
mock.onGet(apiPaths.extensions).reply(200, extensionsGet).onGet(/extensions\/\w+\/settings/).reply(404);


mock.onPost(apiPaths.extensions).reply(() => {
    extensionsCreated++;
    if (extensionsCreated > 6) return [500, 'Shouldnt create more then 4 extensions']
    return [200, {}]
}).onGet(/extensions\/[\w\.]+\/settings/).reply((config) => {
    if (config.url && config.url.indexOf("/Extension.ListenerExtensionId") > -1) {
        listenerExtensionSettingsFetched = true;
        return [200, settingsListenerExtensionIdGet]
    } else if (config.url && config.url.indexOf("/Extension.AnotherId") > -1) {
        extensionSettingsFetched = true;
        return [200, settingsAnotherIdInDaExGet]
    } else {
        return [500];    
    }    
});

mock.onPut(/extensions\/[\w\.]+\/settings/).reply((config) => {
    console.log("Update Settings call was not supposed to happen: " + config.url + " (" + config.method + ")");
    throw new Error('Api call was not supposed to happen: ' + config.url + " (" + config.method + ")");   
});

/*
mock.onPut(/extensions\/[\w\.]+\/settings/).reply((requestConfig) => {
    let setting = JSON.parse(requestConfig.data);
    if (setting?.key === "testFileSetting1") {
        let expectedValue = "these are settings to be sent";
        if (setting?.value !== expectedValue) {
            throw new Error('The testFileSetting1 value was not what was expected. expected: ' + expectedValue + ' found: ' + setting?.value);
        }
        fileSettingCreated = true;
        return [200, {}]
    }
    return [200, {}]
});*/

mock.onPost(/extensions\/[\w\.]+\/settings:reload/).reply(204);
mock.onPut(/extensions\/[\w\.]+\/apikey/).reply(200);       
mock.onPost(/extensions\/[\w\.]+:test/).reply(200, { message: "Success! Extension.ExtensionId is running, version: 1.0.0.0"});
mock.onPost(/extensionmanager:restartservice/).reply(() => {
    if (extensionSettingsFetched && listenerExtensionSettingsFetched) {
        return [200, {}]
    }
    throw new Error('The testFileSetting1 was not created!')
    return [500, 'The testFileSetting1 was not created!'];
});

mock.onAny().reply(function(config) {
    console.log("Api call was not supposed to happen: " + config.url + " (" + config.method + ")");
    throw new Error('Api call was not supposed to happen: ' + config.url + " (" + config.method + ")");            
});



tmr.setInput('packagePath', examplePath);
tmr.setInput('extensionConfigPath', examplePath);
tmr.setInput('apiUrl', 'https://example.com/');
tmr.setInput('apiKey', 'b5101c196bfba8254ee4416691ed97c8');
tmr.setInput('environmentName', 'test');
tmr.setInput('defaultAcceptedTestMessage', '^Success');

tmr.registerMock("./axiosInstance", (apiUrl:string, apiKey:string) => { return axios } );

tmr.run();