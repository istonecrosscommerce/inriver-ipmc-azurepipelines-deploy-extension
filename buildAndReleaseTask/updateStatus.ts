import { apiPaths } from "./apiPaths";
import { getErrorMessage } from "./getErrorMessage";
import { ExtensionResult } from "./interfaces/IExtensionApiResult";
import IExtensionConfig from "./interfaces/IExtensionConfig";

export async function updateStatus(extensionConfig:IExtensionConfig, existingExtension:ExtensionResult | undefined, task:any, axiosInstance:any) {
    if (existingExtension === undefined) {
        
        if (!extensionConfig.isEnabled) {
            console.log(`Disabling ${extensionConfig.extensionId}`);
            await axiosInstance({
                method  : 'post',
                url     : apiPaths.extensionEnabling(extensionConfig.extensionId, extensionConfig.isEnabled)
            });    
        }
        if (extensionConfig.isPaused) {
            console.log(`Pausing ${extensionConfig.extensionId}`);
            await axiosInstance({
                method  : 'post',
                url     : apiPaths.extensionPausing(extensionConfig.extensionId, extensionConfig.isPaused)
            });    
        }
    } else {
        if (existingExtension.status !== undefined && extensionConfig.isEnabled !== existingExtension?.status?.isEnabled) {
            console.log(`${extensionConfig.isEnabled ? "Enabling" : "Disabling"} ${extensionConfig.extensionId}`);
            await axiosInstance({
                method  : 'post',
                url     : apiPaths.extensionEnabling(extensionConfig.extensionId, extensionConfig.isEnabled)
            });    
        }
        if (existingExtension.status !== undefined && extensionConfig.isPaused !== existingExtension?.status?.isPaused) {
            console.log(`${extensionConfig.isPaused ? "Pausing" : "Resuming"} ${extensionConfig.extensionId}`);
            await axiosInstance({
                method  : 'post',
                url     : apiPaths.extensionPausing(extensionConfig.extensionId, extensionConfig.isPaused)
            });    
        }
    }
}