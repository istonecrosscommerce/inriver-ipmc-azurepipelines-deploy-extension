import IExtensionConfig from "./interfaces/IExtensionConfig";
import fs = require("fs");
import path = require("path");
import IExtensionConfigJson from "./interfaces/IExtensionConfigJson";

export function getExtensionConfigs(
  extensionConfigPath: string,
  envName: string,
  defaultAcceptedTestMessage: string,
  excludePattern: string | undefined,
  isRoot: boolean = true
): IExtensionConfig[] {
  let extensionConfigs: IExtensionConfig[] = [];

  let files = fs.readdirSync(extensionConfigPath, { withFileTypes: true });
  files.forEach((file) => {
    if (
      file.name.endsWith("." + envName + ".json") &&
      (extensionConfigPath.endsWith(".config") || isRoot)
    ) {
      extensionConfigs = extensionConfigs.concat(
        parseExtensionConfigs(extensionConfigPath, file.name)
      );
    }
    if (file.isDirectory() && isNotExcluded(file.name, excludePattern)) {
      extensionConfigs = extensionConfigs.concat(
        getExtensionConfigs(
          path.join(extensionConfigPath, file.name),
          envName,
          defaultAcceptedTestMessage,
          excludePattern,
          false
        )
      );
    }
  });

  return extensionConfigs;
}

function isNotExcluded(directoryName:string, excludePattern:string | undefined):boolean {
    if (excludePattern === undefined) return true;
    const pattern = new RegExp(excludePattern);
    return !pattern.test(directoryName);
}

function parseExtensionConfigs(
  directoryPath: string,
  filename: string
): IExtensionConfig[] {
  let extensionConfigs: IExtensionConfig[] = [];
  let parsedSettings = JSON.parse(
    fs.readFileSync(path.join(directoryPath, filename), "utf8")
  );
  if (Array.isArray(parsedSettings)) {
    parsedSettings.forEach((setting) => {
      extensionConfigs.push(
        convertExtensionJsonIntoConfig(setting, directoryPath)
      );
    });
  }
  return extensionConfigs;
}

function convertExtensionJsonIntoConfig(
  extensionConfig: IExtensionConfigJson,
  directoryPath: string
): IExtensionConfig {
  for (
    let index = 0;
    index < Object.keys(extensionConfig.settings).length;
    index++
  ) {
    let currentSettingName = Object.keys(extensionConfig.settings)[index];
    let currentSettingValue = extensionConfig.settings[currentSettingName];
    if (typeof currentSettingValue !== "string") {
      let fileName = currentSettingValue.fileName;
      let fileContent = fs.readFileSync(
        path.join(directoryPath, fileName),
        "utf8"
      );
      extensionConfig.settings[currentSettingName] = fileContent;
    }
  }
  return extensionConfig as IExtensionConfig;
}
