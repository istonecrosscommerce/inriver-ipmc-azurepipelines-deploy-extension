import { apiPaths } from "./apiPaths";

export async function restartService(axiosInstance:any, timeoutAfterRestartService:string | undefined) {
  console.log(`Sending restartservice to extensionManager...`)
  await axiosInstance({
    method  : 'post',
    url     : apiPaths.extensionManagerRestartService()
  });
  console.log(`Checking ${timeoutAfterRestartService} timeoutAfterRestartService`);
  if (timeoutAfterRestartService !== undefined) {      
      const timeoutMilliseconds = parseInt(timeoutAfterRestartService);
      console.log(`Waiting for ${timeoutMilliseconds}ms`);
      await delay(timeoutMilliseconds);
  }
}

function delay(ms: number) {
  return new Promise( resolve => setTimeout(resolve, ms) );
}