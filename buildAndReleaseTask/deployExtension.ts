import { apiPaths } from "./apiPaths";
import { getErrorMessage } from "./getErrorMessage";
import { ExtensionResult, ExtensionStatus } from "./interfaces/IExtensionApiResult";
import IExtensionConfig from "./interfaces/IExtensionConfig";
import { IExtensionResult } from "./interfaces/IExtensionResult";
import { updateSettings } from "./updateSettings";
import { updateStatus } from "./updateStatus";

export async function deployExtension(extensionConfig:IExtensionConfig, existingExtension:ExtensionResult | undefined, task:any, axiosInstance:any):Promise<IExtensionResult> {
  try {
      if (existingExtension === undefined) {
          const extensionCreationModel = {
              "assemblyName": extensionConfig.assemblyName,
              "assemblyType": extensionConfig.assemblyType,
              "extensionId": extensionConfig.extensionId,
              "extensionType": extensionConfig.extensionType,
              "packageId": extensionConfig.packageId
          }
  
          console.log("Creating new extension: ", extensionConfig.extensionId);
          const result = await axiosInstance({
              method  : 'post',
              url     : apiPaths.extensions,
              data    : extensionCreationModel
          });
          existingExtension = result?.data;
      } else {
          console.log(`Extension ${extensionConfig.extensionId} already exists.`);
      }

      await updateStatus(extensionConfig, existingExtension, task, axiosInstance);

      await updateSettings(extensionConfig, task, axiosInstance);

      return { 
          extensionId: extensionConfig.extensionId,
          enabled: extensionConfig.isEnabled,
          deployed: true,
          allowedTestResponses: extensionConfig.allowedTestResponses };
  }
  catch (err: any) {
      let message = getErrorMessage(err);
      task.setResult(task.TaskResult.Failed, message);
      console.log("Error when deploying extension ", extensionConfig.extensionId, message);
      return {
          extensionId: extensionConfig.extensionId,
          enabled: extensionConfig.isEnabled,
          deployed: false,
          allowedTestResponses: extensionConfig.allowedTestResponses
      };
  }    
}