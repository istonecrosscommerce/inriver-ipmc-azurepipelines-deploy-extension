import { apiPaths } from "./apiPaths";
import IExtensionConfig from "./interfaces/IExtensionConfig";

export async function deleteExtension(extensionConfig:IExtensionConfig, axiosInstance:any) {
  console.log("Deleting existing extension ", extensionConfig.extensionId);
      
  try {
      await axiosInstance({
          method  : 'delete',
          url     : apiPaths.extension(extensionConfig.extensionId)
      });
  } catch (err: any) {
      if (err.response?.status !== 200) {
          console.log("Unable to delete extension ", extensionConfig.extensionId, " Error:", err.message);
      }
  }
}