import { AxiosInstance } from "axios";
import * as rax from 'retry-axios';

export function setupInterceptors(axiosInstance: AxiosInstance):AxiosInstance {
    axiosInstance.defaults.raxConfig = {
        instance: axiosInstance
    }
    const interceptorId = rax.attach(axiosInstance);
    return axiosInstance;
}