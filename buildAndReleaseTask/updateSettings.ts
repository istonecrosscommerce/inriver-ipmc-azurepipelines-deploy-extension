import { apiPaths } from "./apiPaths";
import { getErrorMessage } from "./getErrorMessage";
import IExtensionConfig from "./interfaces/IExtensionConfig";

export async function updateSettings(extensionConfig:IExtensionConfig, task:any, axiosInstance:any) {
  try {
      if (extensionConfig.apiKey) {
          await axiosInstance({
              method  : 'put',
              url     : apiPaths.extensionApiKey(extensionConfig.extensionId),
              data    : { "apiKey": extensionConfig.apiKey }
          })            
      }

      let currentSettings:any = await axiosInstance({
          method  : 'get',
          url     : apiPaths.extensionSettings(extensionConfig.extensionId)
      });

      let settingKeysAlreadyUpToDate:string[] = [];

      if (currentSettings?.data?.length > 0) {
          console.log(`Verifying ${currentSettings.data.length} old settings for ${extensionConfig.extensionId} against the new settings...`);
          for (let currentDeleteSettingIndex:number=0; currentDeleteSettingIndex < currentSettings.data.length; currentDeleteSettingIndex++) {
              const settingKey = currentSettings.data[currentDeleteSettingIndex].key;
              const existingValue = currentSettings.data[currentDeleteSettingIndex].value;
              if (existingValue !== extensionConfig.settings[settingKey]) {
                console.log(`Setting: ${settingKey} existing value:${existingValue} was not equal to the new value:${extensionConfig.settings[settingKey]}, deleting it.`);
                await axiosInstance({
                    method  : 'delete',
                    url     : apiPaths.extensionSettingsKey(extensionConfig.extensionId, currentSettings.data[currentDeleteSettingIndex].key)
                });                
              } else {
                settingKeysAlreadyUpToDate.push(settingKey);
              }
          }
      }

      if (settingKeysAlreadyUpToDate.length === Object.keys(extensionConfig.settings).length) {
        console.log(`No settings updated for ${extensionConfig.extensionId}.`);
      } else {
        console.log(`Adding ${Object.keys(extensionConfig.settings).length - settingKeysAlreadyUpToDate.length} settings to ${extensionConfig.extensionId}`);
      }

      for (let index=0; index < Object.keys(extensionConfig.settings).length; index++) {
          let currentSettingName = Object.keys(extensionConfig.settings)[index];
          let currentSettingValue = extensionConfig.settings[currentSettingName];
          if (settingKeysAlreadyUpToDate.indexOf(currentSettingName) === -1) {
            await axiosInstance({
                method  : 'put',
                url     : apiPaths.extensionSettings(extensionConfig.extensionId),
                data    : { "key": currentSettingName, "value": currentSettingValue }
            });              
          }
      }

      await axiosInstance({
          method  : 'post',
          url     : apiPaths.extensionSettingsReload(extensionConfig.extensionId)
      })
  }
  catch (err) {
      let message = getErrorMessage(err);
      task.setResult(task.TaskResult.Failed, message);
      console.log("Error when deploying extension ", extensionConfig.extensionId, message);
      return;
  }
  
}