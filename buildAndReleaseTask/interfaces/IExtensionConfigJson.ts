import IExtensionConfig from "./IExtensionConfig";
import IFileSetting from "./IFileSetting";

export default interface IExtensionConfigJson extends Omit<IExtensionConfig, 'settings'> {
    settings: { [key: string]: string | IFileSetting ; },
}