export interface ExtensionStatus {
    isEnabled: boolean;
    isPaused: boolean;
}

export interface PackageResult {
    id: number;
    createdDate: Date;
    modifiedDate: Date;
    fileName: string;
    version: number;
}

export interface ExtensionResult {
    extensionId: string;
    extensionType: string;
    assemblyName: string;
    assemblyType: string;
    status: ExtensionStatus;
    package: PackageResult;
}
