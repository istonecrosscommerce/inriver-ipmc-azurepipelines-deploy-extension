export interface ITestResult {
  successfullyMatched: boolean,
  message: string,
}