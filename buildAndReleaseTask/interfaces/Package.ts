export default interface Package {
    packageName: string,
    packageId: number,
    deployed: boolean,
    failedToDelete: boolean
    failedPackageId: number
}