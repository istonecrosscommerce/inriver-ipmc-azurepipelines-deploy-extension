export interface IExtensionResult {
  extensionId: string,
  deployed: boolean,
  enabled: boolean,
  allowedTestResponses: string | undefined,
}