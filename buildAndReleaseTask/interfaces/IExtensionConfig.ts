import IFileSetting from "./IFileSetting";

export default interface IExtensionConfig {
    extensionId: string,
    extensionType: string[] | string,
    assemblyName: string,
    assemblyType: string,
    isEnabled: boolean,
    isPaused: boolean,
    allowedTestResponses?: string,
    apiKey: string | undefined,
    packageName: string,
    packageId:number | undefined,
    settings: { [key: string]: string ; },
    notAppendExtensionType?: boolean;
}