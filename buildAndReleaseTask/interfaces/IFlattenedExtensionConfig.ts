import IExtensionConfig from "./IExtensionConfig";

export default interface IFlattenedExtensionConfig extends IExtensionConfig {
    extensionType: string,
}