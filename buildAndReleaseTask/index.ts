import tl = require('azure-pipelines-task-lib/task');
import getAxiosInstance from './axiosInstance';
import { deployExtensions } from './deployExtensions';
import { IExtensionResult } from './interfaces/IExtensionResult';
import { ITestResult } from './interfaces/ITestResults';
import { restartService } from './restartService';
import { testExtensions } from './testExtensions';
import fs = require('fs');
import path = require('path');
import { getExtensionConfigs } from './getExtensionConfigs';
import IExtensionConfig from './interfaces/IExtensionConfig';

async function run() {
    try {
        const extensionConfigPath: string | undefined = tl.getInput('extensionConfigPath', true);
        if (!extensionConfigPath || extensionConfigPath.length === 0) {
            tl.setResult(tl.TaskResult.Failed, 'No extensionConfigPath supplied');
            return;
        }
        if (!fs.existsSync(extensionConfigPath)) {
            tl.setResult(tl.TaskResult.Failed, 'extensionConfigPath ' + extensionConfigPath + ' doesn\'t exist');
            return;
        }

        const packagePath: string | undefined = tl.getInput('packagePath', true);
        if (!packagePath || packagePath.length === 0) {
            tl.setResult(tl.TaskResult.Failed, 'No packagePath supplied');
            return;
        }
        if (!fs.existsSync(packagePath)) {
            tl.setResult(tl.TaskResult.Failed, 'packagePath ' + packagePath + ' doesn\'t exist');
            return;
        }

        const inputApiUrl: string | undefined = tl.getInput('apiUrl', true);
        if (!inputApiUrl || inputApiUrl.length === 0) {
            tl.setResult(tl.TaskResult.Failed, 'No apiUrl supplied');
            return;
        }
        const apiUrl:string = inputApiUrl.endsWith('/') ? inputApiUrl : inputApiUrl + '/';

        const apiKey: string | undefined = tl.getInput('apiKey', true);
        if (!apiKey || apiKey.length === 0) {
            tl.setResult(tl.TaskResult.Failed, 'No apiKey supplied');
            return;
        }
        const instance = getAxiosInstance(apiUrl, apiKey);

        const envName: string | undefined = tl.getInput('environmentName', true);
        if (!envName || envName.length === 0) {
            tl.setResult(tl.TaskResult.Failed, 'No environmentName supplied');
            return;
        }

        const timeoutAfterRestartService: string | undefined = tl.getInput('timeoutAfterRestartService', false);
        const excludePattern: string | undefined = tl.getInput('excludePattern', false);

        let defaultAcceptedTestMessage: string | undefined = tl.getInput('defaultAcceptedTestMessage', false);
        if (defaultAcceptedTestMessage === undefined) defaultAcceptedTestMessage = ""

        let extensionConfig:IExtensionConfig[] = getExtensionConfigs(extensionConfigPath, envName, defaultAcceptedTestMessage, excludePattern);
        
        const result:IExtensionResult[] = await deployExtensions(extensionConfig, packagePath, tl, instance);
        await restartService(instance, timeoutAfterRestartService);
        const testResult:ITestResult[] = await testExtensions(result, defaultAcceptedTestMessage, tl, instance);

        const deployMessage = `Deployed (${result.filter((f) => f.deployed).length}/${result.length}) extensions.`
        const testMessage = `Successfully tested (${testResult.filter((t) => t.successfullyMatched).length}/${testResult.length}) of the deployed extensions.`;
        tl.setResult(tl.TaskResult.Succeeded, `${deployMessage} ${testMessage}`);
    }
    catch (err: any) {
        let message = err.message + err.stack
        tl.setResult(tl.TaskResult.Failed, message);
    }
}

run();