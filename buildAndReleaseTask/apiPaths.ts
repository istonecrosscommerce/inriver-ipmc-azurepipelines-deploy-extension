export const apiPaths = {
    uploadbase64: 'packages:uploadbase64',
    uploadandreplacebase64: (packageId:number) => `packages/${packageId}:uploadandreplacebase64`,
    package: (packageId:number) => `packages/${packageId}`,
    packages: 'packages',
    extension: (extensionId:string) => `extensions/${extensionId}`,
    extensionEnabling: (extensionId:string, enable:boolean) => `${apiPaths.extension(extensionId)}:${enable ? "enable" : "disable"}`,
    extensionPausing: (extensionId:string, pause:boolean) => `${apiPaths.extension(extensionId)}:${pause ? "pause" : "resume"}`,
    extensionApiKey: (extensionId:string) => `${apiPaths.extension(extensionId)}/apikey`,
    extensionSettings: (extensionId:string) => `${apiPaths.extension(extensionId)}/settings`,
    extensionTest: (extensionId:string) => `${apiPaths.extension(extensionId)}:test`,
    extensionSettingsKey: (extensionId:string, settingsKey:string) => `${apiPaths.extension(extensionId)}/settings/${settingsKey}`,
    extensionSettingsReload: (extensionId:string) => `${apiPaths.extension(extensionId)}/settings:reload`,
    extensionManager: `extensionmanager`,
    extensionManagerRestartService: () => `${apiPaths.extensionManager}:restartservice`,
    extensions: 'extensions'
}