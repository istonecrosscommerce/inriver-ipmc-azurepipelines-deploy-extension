import { AxiosInstance } from "axios";
import { setupInterceptors } from "./axiosRetryInterceptor";

const axios = require('axios').default;
axios.defaults.adapter = require('axios/lib/adapters/http');

const getAxiosInstance:any = (apiUrl:string, apiKey:string) => {

    const instance = axios.create({
        baseURL: apiUrl,
        timeout: 120000,
        headers: {'X-inRiver-APIKey': apiKey},
        maxContentLength: Infinity,
        maxBodyLength: Infinity
    });

    setupInterceptors(instance);

    return instance;
}

export default getAxiosInstance;