import { IExtensionResult } from "./interfaces/IExtensionResult";
import { ITestResult } from "./interfaces/ITestResults";
import { testExtension } from "./testExtension";

export async function testExtensions(extensions:IExtensionResult[], defaultAcceptMessage:string, task:any, axiosInstance:any) {
  console.log(`Starting testing of ${extensions.length} extensions...`)
  let testResults:ITestResult[] = [];
  for (let extensionIndex=0; extensionIndex < extensions.length; extensionIndex++) {
      const extension = extensions[extensionIndex];
      if (!extension.deployed || !extension.enabled) continue;
      let testResult = await testExtension(extension, defaultAcceptMessage, axiosInstance);
      testResults.push(testResult);
  }
  const successLength = testResults.filter(t => t.successfullyMatched).length;
  const supposedLength = extensions.filter(ex => ex.deployed && ex.enabled).length;
  if (successLength !== supposedLength) {
    task.setResult(task.TaskResult.SucceededWithIssues, `Only successfully tested (${successLength}/${supposedLength}) of the deployed extensions.`);
  }
  return testResults;
}