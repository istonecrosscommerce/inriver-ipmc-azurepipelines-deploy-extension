export function getErrorMessage(err:any):string {
  let message:string = err.response?.data?.message ? err.response.data.message + " (" + err.response.status + ")" : err.message;
  if (err.isAxiosError === true) {
    message = `Error - ${err.response?.status} - ${JSON.stringify(err.response?.data)} (http ${err.response?.config?.method} - /${err.response?.config?.url})`
  }
  return message;
}