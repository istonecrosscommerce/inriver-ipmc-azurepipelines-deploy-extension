import { ExtensionResult } from "./interfaces/IExtensionApiResult";
import IFlattenedExtensionConfig from "./interfaces/IFlattenedExtensionConfig";

export function getExistingExtension(currentExtension:IFlattenedExtensionConfig, existingExtensions:ExtensionResult[]):ExtensionResult | undefined {    
    if (Array.isArray(existingExtensions)) {
        return existingExtensions.find((elem) => {
            return elem?.extensionId === currentExtension.extensionId;
        });
    }
    return undefined;
}