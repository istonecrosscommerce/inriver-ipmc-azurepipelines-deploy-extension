import path from "path";
import { deployExtension } from "./deployExtension";
import { getExistingExtension } from "./extensionAlreadyExists";
import { flattenExtensionConfig } from "./flattenExtensionConfig";
import { getExtensions } from "./getExtensions";
import IExtensionConfig from "./interfaces/IExtensionConfig";
import { IExtensionResult } from "./interfaces/IExtensionResult";
import Package from "./interfaces/Package";
import { updatePackage } from "./updatePackage";
import fs = require('fs');
import { ExtensionStatus } from "./interfaces/IExtensionApiResult";

export async function deployExtensions(extensionConfigs:IExtensionConfig[], packagePath:string, task:any, axiosInstance:any):Promise<IExtensionResult[]> {
  let extensionResult:IExtensionResult[] = [];

  if (!extensionConfigs || !Array.isArray(extensionConfigs) || extensionConfigs.length === 0) {
      task.setResult(task.TaskResult.Failed, "No extensions found in the extensions config, or is was incorrect formatted");
      return [];
  }

  const files = fs.readdirSync(packagePath, { withFileTypes: true });
  console.log('Finding packages in: ', packagePath);
  let packages:Package[] = new Array();
  
  for (let i:number = 0; i < files.length; i++) {
      if (files[i].isFile() && files[i].name.endsWith(".zip")) {
          packages.push(await updatePackage(path.join(packagePath, files[i].name), files[i].name, axiosInstance));
      }
  }

  let flattenedConfigs = flattenExtensionConfig(extensionConfigs, packages);
  let currentExtensions = await getExtensions(axiosInstance);

  for (let extensionIndex:number = 0; extensionIndex<flattenedConfigs.length; extensionIndex++) {
      let result:IExtensionResult = await deployExtension(flattenedConfigs[extensionIndex], getExistingExtension(flattenedConfigs[extensionIndex], currentExtensions), task, axiosInstance);        
      extensionResult.push(result);
  }

  return extensionResult;
}