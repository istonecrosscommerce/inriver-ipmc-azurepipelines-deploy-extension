import { apiPaths } from "./apiPaths";
import { getErrorMessage } from "./getErrorMessage";
import fs = require('fs');

export async function updatePackage(packageFilePath:string, packageName:string, axiosInstance:any) {
  let packageId = -1;
  let deployed = false;
  let deletedSuccessfully = true;
  let failedPackageId = -1;
  try {
      const contents = fs.readFileSync(packageFilePath, {encoding: 'base64'});

      let existingPackages = await axiosInstance({
          method  : 'get',
          url     : apiPaths.packages
      });

      let packageAlreadyExists:boolean = false;
      let existingId:number = -1;

      if (existingPackages && existingPackages.data && Array.isArray(existingPackages.data)) {
          let foundPackage = existingPackages.data.find((p:any) => p.fileName === packageName );
          if (foundPackage) {
              packageAlreadyExists = true;
              existingId = foundPackage.id;
          }
      }

      failedPackageId = existingId;

      if (packageAlreadyExists) console.log(`Package ${packageName} already exists, replacing...`);
      
      let url = packageAlreadyExists ? apiPaths.uploadandreplacebase64(existingId) : apiPaths.uploadbase64;

      console.log("Sending package to ", url);

      const postData = {
          "fileName": packageName,
          "data": contents
      }

      let res = await axiosInstance({
          method  : 'post',
          url     : url,
          data    : postData,
      });

      

      packageId = res.data.id;
      console.log(`Uploaded package ${packageName} from ${packageFilePath} with ID ${packageId} successfully.`)
      if (!deletedSuccessfully) console.log("Failed when deleting existing package ");

      return { packageName: packageName, packageId: packageId, deployed: deployed, failedToDelete: !deletedSuccessfully , failedPackageId: failedPackageId };
  } catch (err) {     
      let message = getErrorMessage(err);
      console.log("Error when uploading package: ", message);
      return { packageName: packageName, packageId: packageId, deployed: deployed, failedToDelete: !deletedSuccessfully, failedPackageId: failedPackageId };
  }   
}
