import { apiPaths } from "./apiPaths";
import { ExtensionResult } from "./interfaces/IExtensionApiResult";

export async function getExtensions(axiosInstance:any):Promise<ExtensionResult[]> {
  let result = await axiosInstance({
      method  : 'get',
      url     : apiPaths.extensions 
  })

  return result?.data;
}
