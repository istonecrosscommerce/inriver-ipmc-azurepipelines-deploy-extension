import IExtensionConfig from "./interfaces/IExtensionConfig";
import IFlattenedExtensionConfig from "./interfaces/IFlattenedExtensionConfig";
import Package from "./interfaces/Package";

export function flattenExtensionConfig(extensionConfigs:IExtensionConfig[], packages:Package[]):IFlattenedExtensionConfig[] {
    let flattenedConfig:IFlattenedExtensionConfig[] = new Array();
    for (let extensionIndex:number=0; extensionIndex<extensionConfigs.length; extensionIndex++) {
        let currentExtension = extensionConfigs[extensionIndex];
        currentExtension.packageId = packages.find(p => p.packageName === currentExtension.packageName)?.packageId;
        if (currentExtension.packageId === undefined) {
            continue;
        }
        if (Array.isArray(currentExtension.extensionType)) {
            if(currentExtension.extensionType.length > 1 && currentExtension.notAppendExtensionType == true){
                throw new Error("Can't have more then 1 extensionType when notAppendExtensionType is true");
            }
            for (let extensionVariantIndex:number=0; extensionVariantIndex<currentExtension.extensionType.length; extensionVariantIndex++) {
                let currentExtensionVariant = {...currentExtension};
                currentExtensionVariant.extensionId = currentExtensionVariant.extensionId + getShortenedInRiverType(currentExtension.extensionType[extensionVariantIndex], currentExtension.notAppendExtensionType ?? false);
                currentExtensionVariant.extensionType = currentExtension.extensionType[extensionVariantIndex];

                flattenedConfig.push(currentExtensionVariant as IFlattenedExtensionConfig);
            }
        } else {
            currentExtension.extensionId = currentExtension.extensionId + getShortenedInRiverType(currentExtension.extensionType.toString(), currentExtension.notAppendExtensionType ?? false);
            flattenedConfig.push(currentExtension as IFlattenedExtensionConfig);
        }
    }

    return flattenedConfig;
}

function getShortenedInRiverType(extensionType:string, notAppendExtensionType:boolean):string {
    let shortenedType:string = "";
    if(notAppendExtensionType == false){
        for (var i=0; i<extensionType.length; i++) {
            let char = extensionType.charAt(i);
            if (char == char.toUpperCase()) {
                if (i+1 > extensionType.length) {
                    shortenedType = shortenedType + char;
                } else {
                    shortenedType = shortenedType + char + extensionType.charAt(i+1);
                    i++;    
                }
            }
        }   
    }
     
    return shortenedType;
}