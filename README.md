Azure DevOps Task to deploy & test inRiver extensions in iPMC.

## Table of Contents

  - [Features](#features)
  - [Local development](#local-development)
  - [Configuration](#configuration)

## Features

- Make API calls against InRivers [REST API](https://apieuw.productmarketingcloud.com/swagger/ui/index#/)
- Create extensions
    - Generate ExtensionIds based on Id and type, or just use the specified Id
- Clear & update settings
    - Removes all settings from the extension
    - Takes settings from either the config or a seperate file and creates them
- Restart service
- Execute test methods and match the result against specified strings

## Local development

Download and install node version 10.15.1 for your OS:
[node v10.15.1](https://nodejs.org/download/release/v10.15.1/)

You can also use nvm for managing node versions **(Recommended)**
[Windows](https://github.com/coreybutler/nvm-windows)
[MacOS](https://github.com/nvm-sh/nvm)

All the code is under the `/buildAndReleaseTask` directory.

install all modules
```
$ npm install
```

starting typescript compiler
```
$ npx tsc --watch
```

running unittests
```
$ npx mocha .\tests\_suite.js
```

## Configuration

The task will look for these `.ENV.json` files under `.config` folders or in the root folder specified.
Where `ENV` is the specified parameter to the task.

```javascript
// This can either be an array of extensions, or just an extension.
[
    {        
        // `extensionId` is the base extension id to use
        extensionId: "extensionId",

        // `extensionType` is either an array of extensionTypes, or a string. 
        // From these we extract the capital letters and the following letter to append to the extensionType
        // unless `notAppendExtensionType` is set to true.
        // (e.g. extensionIdLiLi, extensionIdEnLi)
	    extensionType: ["LinkListener", "EntityListener"],

        // `assemblyName` is the name of your DLL inside the package to use. 
        assemblyName: "inRiver.Example.dll",

        // `assemblyType` is the namespace of your extension
        assemblyType: "inRiver.Training.Example.Extension",
        
        // `isEnabled` is weather or not this extension should be enabled or not.
        isEnabled: true,

        // `isPaused` is weather or not this extension should be paused or not.
        isPaused: false,

        // `apiKey` is the apiKey to set (only used for InboundDataExtensions)
        apiKey: "exampleApiKey",

        // `apiKey` is the name of your package file in the specified directory.
        packageName: "example.zip",

        // `allowedTestResponses` is a [RegExp](https://www.regular-expressions.info/quickstart.html) that will be override the global setting for this extension
        // This is matched against the Test method on the extension, and if it doesnt match the task will fail.
        allowedTestResponses: "^\\w{7}",
	
        // `settings` is a key/value object that contains all the settings that should be set on the extension.
        // value kan either be a string, or an object with the fileName property, that specifies a file in the same directory
        // as this .json file to be read and used as value instead.
        settings: {
		    testSetting1: "value1",
		    testFileSetting2: {
                fileName: "justafilename.ext"
            }
        },

        // `notAppendExtensionType` is an optional boolean that specifies whether or not we should append extensionType to the extensionId.
        // default: false
        notAppendExtensionType: false
    }
]
```